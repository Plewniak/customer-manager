import { Component } from '@angular/core';
import { Customer } from './model/model';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  customer: Customer = {
    name: ' Jan Kowalski',
    // tslint:disable-next-line: max-line-length
    photoUrl: '../assets/images/kisspng-customer-service-computer-software-business-customer-5abec68714f1f3.3255194515224521030858.png',
    description: 'jakiś opis',
    age: 41,
    address: {
      street: 'Stawowa 147',
      houseNumber: 147,
      city: 'Polanka Wielka'
    }
  }

  isActive: boolean = true;
  color: boolean = true;
  constructor() { }

  changeIsActive() {
    this.isActive = !this.isActive;
  }
  changeColor() {
    this.color = !this.color;
  }
}
